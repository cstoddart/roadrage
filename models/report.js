var mongoose = require('mongoose')
var Schema = mongoose.Schema

var ReportSchema = new Schema({
  license: String,
  description: String
})

module.exports = mongoose.model('Report', ReportSchema)
