var express = require('express')
var mongoose = require('mongoose')
var bodyParser = require('body-parser')

var app = express()
var router = express.Router()

var Report = require('./models/report')

var mongoDB =
  'mongodb://roadrage:X4H2GI2va8srtg9i@roadrage-shard-00-00-dswrl.mongodb.net:27017,roadrage-shard-00-01-dswrl.mongodb.net:27017,roadrage-shard-00-02-dswrl.mongodb.net:27017/roadrage?ssl=true&replicaSet=RoadRage-shard-0&authSource=admin'
mongoose.connect(mongoDB, { useMongoClient: true })
var db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error: '))

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Credentials', 'true')
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET,HEAD,OPTIONS,POST,PUT,DELETE'
  )
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Access-Control-Allow-Headers,Origin,Accept,X-RequestedX-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers'
  )
  res.setHeader('Cache-Control', 'no-cache')
  next()
})

router.get('/', function(req, res) {
  res.send('Hello World')
})

router
  .route('/reports')
  .get(function(req, res) {
    Report.find(function(err, reports) {
      if (err) res.send(err)

      res.json(reports)
    })
  })
  .post(function(req, res) {
    var report = new Report()
    req.body.license ? (report.license = req.body.license) : null
    req.body.description ? (report.description = req.body.description) : null
    
    report.save(function(err, report) {
      if (err) res.send(err)
      res.json( report )
    })
  })

app.use('/api', router)

app.listen(3000, function() {
  console.log('Server started on port 3000')
})
