import React, { Component } from 'react'

import ReportContainer from './components/ReportContainer'

const App = () => {
  return (
    <div>
      <div className="logo">
        <span>Road</span>
        <span>Rage</span>
      </div>
      <ReportContainer />
    </div>
  )
}
export default App
