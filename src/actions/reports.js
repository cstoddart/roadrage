import { ADD_REPORT } from './types'
import axios from 'axios'

export function addReport(report) {
  return {
    type: ADD_REPORT,
    payload: { report }
  }
}

export function uploadReport(report) {
  return dispatch => {
    return axios
      .post('http://localhost:3000/api/reports', report)
      .then(res => { dispatch(addReport(res.data)) })
  }
}

export function getReports() {
  return dispatch => {
    return axios
      .get('http://localhost:3000/api/reports')
      .then(res => {
        res.data.forEach(report => {
          dispatch(addReport(report))
        })
      })
  }
}