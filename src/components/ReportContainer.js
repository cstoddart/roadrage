import React, { Component } from 'react'
import { connect } from 'react-redux'
import Scroll, { scroller } from 'react-scroll'

import { uploadReport, getReports } from '../actions/reports'
import ReportList from './ReportList'
import ReportForm from './ReportForm'

let Link = Scroll.Link
let Element = Scroll.Element

class ReportContainer extends Component {
  componentDidMount() {
    this.props.getReports()
  }

  submit = values => {
    this.props.uploadReport(values)
  }

  render() {
    const { reports } = this.props
    return (
      <div>
        <Link to="rage" smooth="true" className="button">
          Rage!
        </Link>
        <h1 className="section-headline">License Reports</h1>
        <ReportList reports={reports} />
        <Element name="rage" />
        <ReportForm onSubmit={this.submit} />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  reports: state.reports
})

export default connect(mapStateToProps, { uploadReport, getReports })(
  ReportContainer
)
