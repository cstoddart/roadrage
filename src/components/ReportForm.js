import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'

import '../scss/ReportForm.scss'

const renderInput = field => {
  const { touched, error } = field.meta

  return (
    <div className="input-container">
      <input {...field.input} type={field.type} placeholder={field.placeholder} />
      {touched && error && <span className="form-error">{error}</span>}
    </div>
  )
}

const renderTextarea = field => {
  const { touched, error } = field.meta

  return (
    <div className="input-container">
      <textarea {...field.input} type={field.type} placeholder={field.placeholder} />
      {touched && error && <span className="form-error">{error}</span>}
    </div>
  )
}

class ReportForm extends Component {
  render() {
    const { handleSubmit, errors } = this.props
    return (
      <div className="form-container">
        <h1 className="section-headline">Let Out Your Rage</h1>
        <form onSubmit={handleSubmit}>
          <Field 
            name="license" 
            type="text"
            placeholder="License Plate"
            component={renderInput} />
          <Field 
            name="description" 
            type="textarea"
            placeholder="Description"
            component={renderTextarea} />
          <button type="submit" className="button small">Submit</button>
        </form>
      </div>
    )
  }
}

function validate(values) {
  const errors = {}

  if (!values.license) {
    errors.license = "Please enter a license plate number."
  }

  if (!values.description) {
    errors.description = "Please enter a description."
  }

  return errors
}

ReportForm = reduxForm({
  form: 'reportForm',
  validate
})(ReportForm)

export default ReportForm
