import React, { Component } from 'react'
import { Fade } from 'react-reveal'

import '../scss/ReportList.scss'

export default class ReportList extends Component {
  render() {
    const { reports } = this.props

    return (
      <ul className="report-list">
        {reports.map(report => {
          return (
            <Fade className="report">
              <li key={report._id}>
                <div className="license-container">
                  <div className="license">
                    <span>{report.license}</span>
                  </div>
                </div>
                <div className="description">{report.description}</div>
              </li>
            </Fade>
          )
        })}
      </ul>
    )
  }
}
