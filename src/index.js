import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, compose, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from './reducers'
import thunk from 'redux-thunk'
import 'typeface-passion-one'
import 'typeface-roboto'

import App from './App'
import './scss/index.scss'

const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
)

const ReduxApp = (
  <Provider store={store}>
    <App />
  </Provider>
)

ReactDOM.render(ReduxApp, document.getElementById('root'))
