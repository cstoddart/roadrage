import { combineReducers } from 'redux'
import { reducer as formReducer} from 'redux-form'
import reportsReducer from './reports'

export default combineReducers({
  reports: reportsReducer,
  form: formReducer
})