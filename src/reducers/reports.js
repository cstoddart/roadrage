import { ADD_REPORT } from '../actions/types'

export default (state = [], action) => {
  const { type, payload } = action

  switch (type) {
    case ADD_REPORT: {
      return [...state, payload.report]
    }
  }

  return state
}
